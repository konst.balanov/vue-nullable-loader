## Vue nullable loader (?.)
This package allows you to use [optional chaining operator (**?.**)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining) in Vue components.


## Installation

```
npm i --save-dev vue-nullable-loader 
```

## Example
The [optional chaining operator (**?.**)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining) enables you to read the value of a property located deep within a chain of connected objects without having to check that each reference in the chain is valid.

But Vue components doesn't support this operator. Normally code below will throw `Module build failed: SyntaxError: Unexpected token`
``` html
<template>
  <div v-if="obj.inner_obj1?.message">
    {{ obj?.inner_obj1?.message }}
  </div>
</template>
```
``` html
<script>
  export default {
    computed: {
      objMessage() {
        return this.obj?.inner_obj1?.message
      }
    }
  }
</script>
```
With this loader your code will be transformed to:
```html
<template>
  <div v-if="obj.inner_obj1 && obj.inner_obj1.message">
    {{ obj && obj.inner_obj1 && obj.inner_obj1.message }}
  </div>
</template>
```
```html
<script>
  export default {
    computed: {
      objMessage() {
        return this.obj && this.obj.inner_obj1 && this.obj.inner_obj1.message
      }
    }
  }
</script>
```

## Usage
In your `webpack.*.js` file add `vue-nullable-loader` to `/\.vue$/` loading rules. 
``` js
module.exports = {
  module: {
    rules: [
      // Variant 1
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        // options: ... 
      },
      {
        test: /\.vue$/,
        loader: 'vue-nullable-loader',
      }
...
      // Variant 2
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader',
            // options: ...
          },
          'vue-nullable-loader'
        ]
      }
    ]
  }
}
```
Note that `vue-nullable-loader` must go after `vue-loader`.