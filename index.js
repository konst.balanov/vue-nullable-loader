module.exports = function (content) {
  
  function replacer(match, p1, p2) {
    p1 = p1.trim();
    p2 = p2.trim();
    let retValue;
    if (p2.startsWith('[') && p2.endsWith(']')) {
      retValue = `${p1} && ${p1?.replace(/\?\./g, '.')}${p2}`
    } else {
      retValue = `${p1} && ${p1?.replace(/\?\./g, '.')}.${p2}`
    }
    return retValue
  }

  function nullableOperatorsCount(content) {
    return content?.split('?.').length - 1
  }

  const reg = /([^,;\'\"\(\) \{\}]+)\?\.((?:\w+)?(?:\[.*\]|\(.*\))?)/;

  let prevNullablesCount = nullableOperatorsCount(content);

  while (nullableOperatorsCount(content) !== 0) {
    content = content.replace(reg, replacer)
    let newNullablesCount = nullableOperatorsCount(content);
    if (newNullablesCount === prevNullablesCount)
      throw new Error('vue-template-nullable-loader. Probably wrong syntax with ?.')

    prevNullablesCount = newNullablesCount;
  }

  return content;

}
